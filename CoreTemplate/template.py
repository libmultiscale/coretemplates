#!/usr/bin/env python3

"""
This modules loads the templates and allow various
projections to be made with a fine control over
the variation along the dislocation line
"""

from __future__ import print_function
################################################################
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.tri as mtri
from . import barnett
from scimath.units.api import UnitArray
from scimath.units.unit_parser import unit_parser
parse_unit = unit_parser.parse_unit
################################################################


class Crystal:
    def __init__(self, A=None, atomic_volume=None, **kwargs):
        self.A = A
        self.atomic_volume = atomic_volume

    def __str__(self):
        return f"""
  A: {self.A}
  atomic_volume: {self.atomic_volume}
"""


class CoreTemplate:

    def __init__(self, **kwargs):
        self.crystal = Crystal(**kwargs)
        self.filename = None
        self.original_case = None

        self.centro = None
        self.x0 = None
        self.disp = None
        self.pe_atom = None
        self.correction = None

    def __setitem__(self, key, item):
        self.__dict__[key] = item

    def __getitem__(self, key):
        return self.__dict__[key]

    @staticmethod
    def loadMDCase(A, Va, angle, dirname, step=9999):
        filename = 'md-structure-{0}.npz'.format(step)
        case_dirname = os.path.join(dirname, 'Case{0}'.format(angle))
        atom_file = os.path.join(case_dirname, 'paraview', filename)
        atom_file = os.path.expanduser(atom_file)
        case_dirname = os.path.expanduser(case_dirname)
        case = CoreTemplate(A=A, atomic_volume=Va)
        case.loadAtoms(atom_file)
        return case

    @staticmethod
    def load(filename):
        filename = os.path.expanduser(filename)
        data = np.load(filename, allow_pickle=True)
        A = data['A']
        Va = data['atomic_volume']
        case = CoreTemplate(A=A, atomic_volume=Va)
        case.loadAtoms(filename)
        return case

    def loadAtoms(self, filename):
        self.filename = filename
        data = np.load(self.filename)
        self.x0 = data['mdpos']
        self.disp = data['truedisp']
        self.centro = data['centro']
        self.pe_atom = data['pe_atom']
        if 'correction' in data.files:
            self.correction = data['correction']

    def extractCore(self, radius):
        x0 = self.x0
        disp = self.disp
        pe_atom = self.pe_atom
        centro = self.centro
        # select atoms which are not exactly at the center (0,0,0)
        sel = np.sqrt(x0[:, 0]**2+x0[:, 1]**2+x0[:, 2]**2) > 1e-4
        x0 = x0[sel, :]
        disp = disp[sel, :]
        pe_atom = pe_atom[sel, :]
        sel = np.sqrt(x0[:, 1]**2+x0[:, 2]**2) < radius
        x0 = x0[sel, :]
        disp = disp[sel, :]
        pe_atom = pe_atom[sel, :]
        centro = centro[sel, :]

        # forge the subcase
        sub_case = CoreTemplate()
        sub_case.original_case = self
        sub_case.x0 = x0
        sub_case.crystal = self.crystal

        sub_case.disp = disp
        sub_case.pe_atom = pe_atom
        sub_case.centro = centro

        sub_case.crystal = self.crystal
        sub_case.filename = self.filename

        return sub_case

    def extractProjectedCore(self, radius):
        sub_case = self.extractCore(radius)
        sub_case.templateCorrectionDisp()
        sub_case = sub_case.projectCase()
        return sub_case

    def computeCoreEnergy(self):
        pe_atom = self.pe_atom
        energy = np.average(pe_atom)
        return energy

    def templateCorrectionDisp(self):
        # make a square loop
        dirname = os.path.split(os.path.dirname(self.filename))[0]
        compute_dislo_displacements = barnett.readScriptLoop(dirname)

        # compute pure barnett solution
        disp = self.disp
        x0 = self.x0

        disp2 = compute_dislo_displacements(x0=x0, A=self.crystal.A)
        self.correction = disp-disp2
        return self.correction

    def templateCorrectionError(self):
        diff = self.templateCorrectionDisp()
        error = np.sqrt(diff**2).sum()*self.crystal.atomic_volume
        return error

    def getRadiusInfluence(self, radii):

        res = {'radii': [], 'epot': [], 'error': []}
        for rad in radii:
            sub_case = self.extractCore(rad)

            epot = sub_case.computeCoreEnergy()
            error = sub_case.templateCorrectionError()
            res['radii'].append(rad)
            res['epot'].append(epot)
            res['error'].append(error)
        res['radii'] = UnitArray(res['radii'], units=parse_unit('angstrom'))
        res['epot'] = UnitArray(res['epot'], units=parse_unit('eV'))
        res['error'] = UnitArray(res['error'], units=parse_unit('angstrom**4'))
        return res

    def available_field_list(self):
        f_list = [k for k in self.__dict__
                  if isinstance(self.__dict__[k],
                                np.ndarray)]

        f_list = [f for f in f_list if len(self.__dict__[f].shape) > 0]
        f_list = [f for f in f_list
                  if self.__dict__[f].shape[0] == self.x0.shape[0]]
        return f_list

    def _get_field(self, field, make_it_scalar=False, axis=None):
        if isinstance(field, str):
            try:
                field = self.__dict__[field]
            except Exception as e:
                raise Exception('it seems that {0}'.format(field)
                                + 'is not a field of this core '
                                + 'available is: {0}'.format(
                                    self.available_field_list())
                                + ' ' + str(e))

            if axis is not None:
                field = field[:, axis]

            if len(field.shape) > 1 and field.shape[1] > 1 and make_it_scalar:
                field = np.einsum('ai,ai->a', field, field)
                field = np.sqrt(field)

        if make_it_scalar and (field is not None):
            field = field.flatten()

        return field

    def plot3DAtomField(self, field,
                        axis=None,
                        s=0.5,
                        colorbar=True,
                        depthshade=True,
                        angle=None,
                        cbar_nticks=4):
        fig = plt.figure()
        axe = fig.add_subplot(111, projection='3d')
        x0 = self.x0
        disp = self.disp
        pos = x0 + disp
        field = self._get_field(field, make_it_scalar=True, axis=axis)

        img = axe.scatter(pos[:, 0], pos[:, 1], pos[:, 2],
                          lw=s, c=field, depthshade=depthshade)
        axe.set_xlabel(r'\n$x$ $[\AA]$')
        axe.set_ylabel(r'\n$y$ $[\AA]$')
        axe.set_zlabel(r'\n$z$ $[\AA]$')
        if colorbar:
            fig.cbar = plt.colorbar(img)
            tick_locator = ticker.MaxNLocator(nbins=cbar_nticks)
            fig.cbar.locator = tick_locator
            fig.cbar.update_ticks()

        if angle is not None:
            axe.set_title('\theta = ${0}$'.format(angle))

        plt.tight_layout(pad=1.0, w_pad=1.0, h_pad=1.0)
        return fig

    def plotAtomField(self, field,
                      colorbar=True,
                      s=14.,
                      angle=None,
                      axis=None,
                      norm=None):

        fig = plt.figure()
        axe = fig.add_subplot(111)

        x0 = self.x0
        disp = self.disp
        pos = x0 + disp[:, 1:3]

        field = self._get_field(field, make_it_scalar=True, axis=axis)

        # print(pos.shape)
        # print(field.shape)

        img = axe.scatter(pos[:, 1], pos[:, 0], s=s, lw=0, c=field, norm=norm)
        axe.set_xlabel(r'$z$ $[\AA]$')
        axe.set_ylabel(r'$y$ $[\AA]$')
        if colorbar:
            fig.cbar = fig.colorbar(img)
        if angle is not None:
            axe.set_title('\theta = ${0}$'.format(angle))
        return fig

    def _get_range(self):
        points = self.x0
        _min = points.min(axis=1)
        _max = points.max(axis=1)

        _range = _max-_min
        return _min, _max, _range

    def extractMesh(self):
        points = self.x0
        if points.shape[1] == 3:
            raise Exception('cannot extract mesh from the 3d set of points')

        import scipy.spatial
        conn = scipy.spatial.Delaunay(points).simplices
        # print(conn.shape)

        triangles = mtri.Triangulation(points[:, 1], points[:, 0],
                                       triangles=conn)

        return triangles

    def write(self, name, **kwargs):
        triangles = self.extractMesh()
        output = {
            'triangles': triangles.triangles,
            'points': self.x0,
            'correction': self._get_field(
                'correction'),
            'mdpos': self._get_field('x0'),
            'truedisp': self._get_field('disp'),
            'centro': self._get_field('centro'),
            'pe_atom': self._get_field('pe_atom'),
            'A': self.crystal.A,
            'atomic_volume': self.crystal.atomic_volume
        }
        np.savez_compressed(name, **output)

    def plotMesh(self,
                 colorscale=None,
                 colorbar=True,
                 colorrange=None,
                 angle=None, subdiv=4,
                 axis=None,
                 cbar_nticks=4,
                 nlevels=20):

        triangles = self.extractMesh()
        colorscale = self._get_field(colorscale,
                                     make_it_scalar=True,
                                     axis=axis)

        fig = plt.figure()
        axe = fig.add_subplot(111)

        if colorscale is None:
            axe.triplot(triangles, '-', lw=1., c='black')

        if colorscale is not None:

            if len(colorscale.shape) > 1:
                colorscale = colorscale.flatten()

            refiner = mtri.UniformTriRefiner(triangles)
            tri_refi, z_test_refi = refiner.refine_field(colorscale,
                                                         subdiv=subdiv)

            if colorrange is None:
                _range = colorscale.max() - colorscale.min()
                _min = colorscale.min()-0.1*_range
                _max = colorscale.max()+0.1*_range
                if _min == _max:
                    _min -= _min/2.
                    _max += _max/2.
            else:
                _min, _max = colorrange

            levels = np.linspace(_min, _max, nlevels)
            img = plt.tricontourf(tri_refi, z_test_refi, levels=levels)
            axe.triplot(triangles, '-', lw=.1, c='black')
            if colorbar:
                cbar = fig.colorbar(img)
                fig.cbar = cbar
                fig.colorrange = (_min, _max)
                tick_locator = ticker.MaxNLocator(nbins=cbar_nticks)
                fig.cbar.locator = tick_locator
                fig.cbar.update_ticks()

        axe.set_xlabel(r'$z$ $[\AA]$')
        axe.set_ylabel(r'$y$ $[\AA]$')
        if angle is not None:
            axe.set_title('\theta = ${0}$'.format(angle))
        return fig

    def projectCase(self):
        projected_case = CoreTemplate()
        projected_case.crystal = self.crystal
        projected_case.original_case = self

        f_list = self.available_field_list()

        for f_name in f_list:
            x0, avg, std, counts = self._compute_projected_stats(f_name)
            if f_name != 'x0':
                projected_case[f_name] = avg
            else:
                projected_case.x0 = x0.copy()
                projected_case.counts = counts

            projected_case['std_'+f_name] = std

        return projected_case

    def _compute_projected_stats(self, field):

        field = self._get_field(field)
        if self.x0.shape[1] != 3:
            raise Exception('probably this case was already projected')
        x0 = self.x0
        tmp = x0[:, 1:3]
        x0 = np.array(tmp[:, 0], complex)
        x0.imag = tmp[:, 1]

        un, inv, counts = np.unique(x0.round(decimals=2),
                                    return_inverse=True,
                                    return_counts=True)

        shp = field.shape[1]
        avg = np.zeros((un.shape[0], shp))
        std = np.zeros((un.shape[0], shp))
        for i, gi in enumerate(inv):
            avg[gi, :] += field[i, :]
            std[gi, :] += field[i, :]*field[i, :]
        avg = np.einsum('aj,a->aj', avg, 1./counts)
        std = np.einsum('aj,a->aj', std, 1./counts)
        std[:] -= np.einsum('ai,ai->ai', avg, avg)
        std = np.abs(std)
        std = np.sqrt(std)
        x0 = np.zeros((un.shape[0], 2))
        x0[:, 0] = un[:].real
        x0[:, 1] = un[:].imag
        return x0, avg, std, counts

    def saveAsCSV(self, filename):
        import csv
        fname_list = self.available_field_list()
        print(fname_list)
        f_list = [self._get_field(f) for f in fname_list]
        fieldnames = []
        for fname, f in zip(fname_list, f_list):
            sz = f.shape[1]
            for i in range(0, sz):
                fieldnames.append('{0}_{1}'.format(fname, i))
        csvfile = open(filename, 'w')
        writer = csv.writer(csvfile)
        writer.writerow(fieldnames)

        n = f_list[0].shape[0]
        for i in range(0, n):
            row = []
            for f in f_list:
                row += list(f[i, :])
            writer.writerow(row)
        csvfile.close()

    def __str__(self):
        return f"""
crystal: {self.crystal}
filename: {self.filename}
original_case: {self.original_case}
"""

################################################################


def templateCorrectionErrorVolterra(radius, x0, disp, pe_atom, burg, r0):
    x0, disp, pe_atom = CoreTemplate.extractCore(radius, x0, disp, pe_atom)

    import template
    disp2 = template.computeVolterraDisplacements(x0, burg)*float(r0)
    error = np.sqrt((disp-disp2)**2).sum()
    return error

################################################################


if __name__ == "__main__":
    core = CoreTemplate.loadCase(90, './offline_calculation/')
    sub_case = core.extractCore(20)
    print(sub_case.__dict__)
