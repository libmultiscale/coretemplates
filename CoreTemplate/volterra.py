#!/usr/bin/env python

"""
Volterra tools:

everything is computed for
- the line direction to be X
- the pack plane direction to be Y
- the edge direction to be Z
"""

################################################################
import numpy as np
################################################################


def computeVolterraZDisplacement(X, burgers, pois):

    b_edge = burgers.dot([0, 0, 1])
    x = - X[:, 2]
    y = X[:, 1]
    r2 = x*x + y*y
    epsilon = 1e-15
    res = b_edge/(2.*np.pi)*(np.arctan2(y, x) + x*y/(2.*(1-pois)*(r2+epsilon)))
    return res

################################################################


def computeVolterraYDisplacement(X, burgers, pois):

    x = - X[:, 2]
    y = X[:, 1]

    x_2 = x*x
    y_2 = y*y
    b_edge = burgers.dot([0, 0, 1])
    b_edge2 = b_edge*b_edge
    r2 = x_2+y_2
    epsilon = 1e-15
    c1 = (1-2.*pois)
    c2 = 4.*(1.-pois)
    res = -b_edge/(2.*np.pi)*(c1/c2*np.log((r2+epsilon)/b_edge2)
                              + (x_2 - y_2)/c2/(r2+epsilon))

    return res

################################################################


def computeVolterraXDisplacement(X, burgers):

    x = X[:, 2]
    y = X[:, 1]

    b_screw = burgers.dot([1, 0, 0])
    res = b_screw/(2.*np.pi)*(np.arctan2(y, x))
    return res

################################################################


def computeVolterraDisplacements(X, burgers, pois=.3):
    res = np.zeros((X.shape[0], 3))
    res[:, 0] = computeVolterraXDisplacement(X, burgers)
    res[:, 1] = computeVolterraYDisplacement(X, burgers, pois)
    res[:, 2] = computeVolterraZDisplacement(X, burgers, pois)
    return res
