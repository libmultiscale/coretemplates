#!/usr/bin/env python
import sys
import os
import shutil
import numpy as np

sys.path.append('./')
from default_parameters import *

################################################################
def createDirectory(param):
    try:
        os.mkdir(param.dirname)
        os.mkdir(param.dirname+"/paraview")
    except Exception as e:
        pass
    return None

################################################################
def createLibFile(param):
    name = param.dirname+'/'+param.lib_filename
    _file = open(os.path.join('templates', param.lib_filename))
    buf = _file.read()
    _file.close()
    content = buf.format(**param.__dict__)
    file0 = open(name, 'w')
    file0.write(content)
    file0.close()
    return None

################################################################
def createLammpsFile(param):
    name = os.path.join(param.dirname, param.lmp_filename)
    _file = open(os.path.join('templates', param.lmp_filename))
    buf = _file.read()
    _file.close()
    content = buf.format(**param.__dict__)
    file0 = open(name, 'w')
    file0.write(content)
    file0.close()

    src_potential_file = os.path.join('templates', param.potential_file)
    dst_potential_file = os.path.join(param.dirname, param.potential_file)
    print os.getcwd(), src_potential_file, dst_potential_file
    shutil.copy(src_potential_file, dst_potential_file)
    return None

################################################################
def createBarnettScript(param):

    slip_in_x = -np.sin((param.ang-90.0)*np.pi/180.0)
    slip_in_y = 0.0
    slip_in_z = np.cos((param.ang-90.0)*np.pi/180.0)
    burgers_vec = [slip_in_x, slip_in_y, slip_in_z]
    slipvec = [0.0, 1.0, 0.0]

    #p4+-----------------------+p3    z
    #  |    +------------+     |      ^
    #  |    |     MD     |     |      |
    #  +-----------------------+      +--->x
    #  p1   |            |     p2
    #       +------------+
    #

    posy = -param.DY*param.A0/6.0

    burgers_vec = [slip_in_x, slip_in_y, slip_in_z]

    point1 = [-1.0*param.dd_boxsize+param.minseg, posy, 0.0]
    point2 = [1.0*param.dd_boxsize-param.minseg, posy, 0.0]
    point3 = [1.0*param.dd_boxsize-param.minseg, posy, param.dd_boxsize-param.minseg]
    point4 = [-1.0*param.dd_boxsize+param.minseg, posy, param.dd_boxsize-param.minseg]

    #points = [point1, point2, point3, point4]
    points = [point4, point3, point2, point1]
    points = np.array(points)

    #write the file
    name = os.path.join(param.dirname, param.barnett_filename)
    _file = open(os.path.join('templates', param.barnett_filename))
    buf = _file.read()
    _file.close()
    content = buf.format(points=repr(points), burg=repr(burgers_vec))
    file0 = open(name, 'w')
    file0.write(content)
    file0.close()

    src_potential_file = os.path.join('templates', 'dislo_template.py')
    dst_potential_file = os.path.join(param.dirname, 'dislo_template.py')
    shutil.copy(src_potential_file, dst_potential_file)
    src_potential_file = os.path.join('templates', 'python_dump.py')
    dst_potential_file = os.path.join(param.dirname, 'python_dump.py')
    shutil.copy(src_potential_file, dst_potential_file)

    return points, burgers_vec, slipvec

###################################################################
def main():

    ###########user define ########################################
    angs = [90, 109.107, 120, 130.893, 139.107, 150, 160.893, 180]
    for ang in angs:
        print ang

        ###############################################################
        param = system_parameters()
        dirname = 'Case'+str(ang)
        quename = 'c'+str(ang)
        ###############################################################

        param.set_dirname(dirname)
        param.RCUT = 12.0
        createDirectory(param)
        param.set_angle(ang)
        param.set_cwd(cwd0)
        param.set_jobname(quename)

        createBarnettScript(param)

        createLammpsFile(param)
        createLibFile(param)

if __name__ == '__main__':
    cwd0 = os.getcwd()
    main()
