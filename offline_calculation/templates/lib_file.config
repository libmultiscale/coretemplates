#MAIN conf
Section    MultiScale MetalUnits

#main
DIMENSION	3
UNITCODE 	MetalUnits
COM 		DISTRIBUTED
LET A = {A0}

#include the geometries
INCLUDE	{lib_filename} Geometries

#declare domains
PROCESSORS	md 6
MD_CODE 	LAMMPS md        

#filter the pad atoms
FILTER Fpad       GEOM     INPUT md GEOMETRY pad           

#compute and impose the displacement due to the dislocation
COMPUTE mdpos0   EXTRACT INPUT md     FIELD position0
COMPUTE truedisp TRUEDISP INPUT md PBC 1 0 0
COMPUTE disploop PYTHON INPUT mdpos0 FILENAME apply_barnett
STIMULATION imposing FIELD INPUT md FIELD displacement COMPUTE disploop ONESHOT 1

#fix the pad atoms
STIMULATION Fix0 RAZ INPUT Fpad FIELD velocity COEF 0.0 FIELD force COEF 0.0 END 10000

#stimulation for the relaxation
STIMULATION relax0 RAZ INPUT md FIELD velocity COEF 0.5   START 2     END 20
STIMULATION relax1 RAZ INPUT md FIELD velocity COEF 0.8   START 20    END 200
STIMULATION relax2 RAZ INPUT md FIELD velocity COEF 0.85  START 200   END 1000
STIMULATION relax3 RAZ INPUT md FIELD velocity COEF 0.9   START 1000  END 2000
STIMULATION relax4 RAZ INPUT md FIELD velocity COEF 0.925 START 2000  END 4000
STIMULATION relax5 RAZ INPUT md FIELD velocity COEF 0.95  START 4000  END 6000
STIMULATION relax6 RAZ INPUT md FIELD velocity COEF 0.98  START 6000  END 10000

#output
COMPUTE centro1 CENTRO INPUT md PBC 1 0 1 RCUT 3.7
DUMPER ini_md PARAVIEW INPUT md DISP VEL FORCE P0 ADDFIELD centro1 ADDFIELD truedisp PREFIX paraview END 5
DUMPER fin_md PARAVIEW INPUT md DISP VEL FORCE P0 ADDFIELD centro1 ADDFIELD truedisp PREFIX paraview START 9995 END 10000
#DUMPER restart     RESTART  INPUT md PREFIX paraview
#DUMPER lammps-data LAMMPS   INPUT md PREFIX paraview

# extract the information for the txt output
COMPUTE yop PYTHON INPUT mdpos0 FILENAME python_dump ADD_COMPUTE truedisp ADD_COMPUTE centro1 ADD_COMPUTE patom GATHER
DUMPER fin_md_structure TEXT INPUT yop PREFIX paraview START 9990 END 10000
DUMPER ini_md_structure TEXT INPUT yop PREFIX paraview START 1 END 10

endSection
    
#LAMMPS conf
Section LAMMPS:md MetalUnits

LAMMPS_FILE {lmp_filename}
CREATE_HEADER
BOUNDARY p s s
LATTICE fcc
LATTICE_SIZE {A0}
REPLICA -{RX}/2 {RX}/2 -{RY}/2 {RY}/2 -{RZ}/2 {RZ}/2
LATTICE_ORIGIN .01 .01 .01
LATTICE_ORIENTX  {lx1} {lx2} {lx3}
LATTICE_ORIENTY  {ly1} {ly2} {ly3}
LATTICE_ORIENTZ  {lz1} {lz2} {lz3}
SPACING {DX} {DY} {DZ}
DOMAIN_GEOMETRY initial

endSection

#GEOMETRIES
Section Geometries MetalUnits
LET INF = 1e300

GEOMETRY initial CUBE  BBOX -{LX}/2 {LX}/2 -{LY}/2 {LY}/2 -{LZ}/2 {LZ}/2
GEOMETRY total    CUBE  BBOX -{LX}/2 {LX}/2 -{LY}/2 {LY}/2 -{LZ}/2 {LZ}/2
GEOMETRY ddtot    CUBE  BBOX  -{dd_boxsize} {dd_boxsize} -{dd_boxsize} {dd_boxsize} -{dd_boxsize} {dd_boxsize}
GEOMETRY pad_top  CUBE  BBOX  -INF INF {LY}/2-{RCUT} INF -INF INF
GEOMETRY pad_bot  CUBE  BBOX  -INF INF -INF -{LY}/2+{RCUT} -INF INF
GEOMETRY pad_left CUBE  BBOX  -INF INF -INF INF -INF -{LZ}/2+{RCUT}
GEOMETRY pad_right  CUBE BBOX  -INF INF -INF INF {LZ}/2-{RCUT} INF
GEOMETRY pad_tb   UNION IDS pad_top    pad_bot
GEOMETRY pad_lr   UNION IDS pad_left   pad_right    
GEOMETRY pad      UNION IDS pad_tb pad_lr

endSection
