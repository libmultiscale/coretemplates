#!/usr/bin/env python

import numpy as np
from numpy import array
import sys
from dislo_template import *

def compute(**kwargs):

    A = kwargs['A']
    r0 = A*np.sqrt(2.0)/2.0
    x0 = kwargs['mdpos0'].copy()
    corners = np.zeros((4, 3))
    corners = np.array({points})

    nodes, edges = createDDSquareLoop(corners, [3, 3, 3, 3])
    burg = np.array({burg})
    
    # compute pure barnett solution
    disp =  computeDisloDisplacements(nodes, edges, x0, burg, slip_plane=[0., 1., 0.])*float(r0)
    return disp
