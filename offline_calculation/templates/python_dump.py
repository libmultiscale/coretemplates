#!/usr/bin/env python

import numpy as np
import sys

def compute(**kwargs):
    print '{0}: treat {1} atoms'.format(kwargs['compute_name'], kwargs['centro1'].shape[0])
    step = kwargs['step']
    centro = kwargs['centro1']
    mdpos = kwargs['mdpos0']
    truedisp = kwargs['truedisp']
    patom = kwargs['patom']
    np.savez('paraview/md-structure-{0}.npz'.format(step), centro=centro, mdpos=mdpos, truedisp=truedisp, pe_atom=patom)
    return centro
