#!/usr/bin/env python
import sys
import os
import operator
from numpy import *
import fileinput
from scipy import stats as stats
#create header for q-file.
#input: machine,absolute_filename(!without fileformat!),que_name,working_directory,walltime_in_hour,arg for mpi

def yes(prompt = 'Please enter Yes/No: '):
    while True:
        try:
            i = raw_input(prompt)
        except KeyboardInterrupt:
            return False
        if i.lower() in ('yes','y'): return True
        elif i.lower() in ('no','n'): return False
    
def createheaderQfile(machine,filename,qname,working_dir,hour,mpi_args):
    if(machine == 'lsms') or (machine == "LSMS"):
        header='#!/bin/bash\n|#$ -S /bin/sh\n|#$ -N '+qname+'\n|#$ -wd '+working_dir+'/'+'\n|#$ -j y\n|#$ -l h_rt='+str(int(hour))+':00:00\n|'
        if(mpi_args != 'none'):
            header = header+'#$ '+mpi_args+'\n|'
        header=header+'LibMS=/home/jcho/LibMultiScale/trunk/build/clients/AMEL'
        headers = header.split('|')
    elif (machine == 'bella') or (machine == 'BELLA') or (machine == 'bellatrix'):
        header1='#!/bin/bash\n|#PBS -N '+qname+'\n|#PBS -S /bin/sh\n|#PBS -o output.'+filename+'\n|#PBS -e error.'+filename+'\n|#PBS -m ae\n|#PBS -M jaehyun.cho@epfl.ch\n|#PBS -l walltime='+str(int(hour))+':00:00\n|'
        if(mpi_args != 'none'):
            header1=header1+mpi_args+'\n|'
        header2='source /opt/software/modules/3.2.10/Modules/3.2.10/init/bash\n|module load intel/13.0.1\n|module load intelmpi/4.1.0\n|module load fftw/3.3.3-intel_intelmpi-13.0.1_4.1.0\n|module load boost/1.53.0-gcc-4.7.2\n|module load cmake/2.8.11\n|module load gmp/5.1.0\n|module load mpfr/3.1.1\n|module load mpc/1.0.1\n|module load gsl/1.15-intel-13.0.1\n|module load python_2.7.3-ALL\n|'
        header3='cd $PBS_O_WORKDIR\n|LibMS="/home/jcho/libmultiscale/trunk/build/clients/AMEL"'
        header = header1+header2+header3
        headers = header.split('|')
    else:
        sys.exit('no machine defined...')

    filename = filename+'.sub'
    tmp = open('TMP', 'w')
    orig = open(filename, 'r')
    tmp.write(''.join(headers) + '\n')
    for line in orig.readlines():
        tmp.write(line)
    orig.close()
    tmp.close()
    os.remove(filename)
    os.rename('TMP', filename)
    return 1
                    
def search(X0,X):
    if(len(X0[0])!=len(X[0]) or len(X0[1])!=len(X[1]) or len(X0[2])!=len(X[2])):
        sys.exit("two different data size...")

    N = len(X0[0])
    low = 0
    high = N - 1
    mid=0
    
    if (N == 0):
        sys.exit("no data...")
        
    while (low <= high):
	mid = int((low+high)/2)
	cmp = comparison(v1[mid],el)
        if (cmp > 0):
            if (mid == 0):
                break
            high = mid - 1          
	elif(cmp < 0):
            low = mid + 1
	else:
            return mid
        
    sys.exit("something weird...")


def comparison(X0,X):
    d = 0.
    for i in range(0,3):
        dist = X0[i]-X[i]
        d += dist*dist
    if (d < .000001**2):
        return 0
    else:
        for i in range(0,3):
            if (abs(X0[i] - X[i]) > .000001**2):
                if (X0[i] < X[i]):
                    return -1
                if (X0[i] > X[i]):
                    return  1
    return 0


def find_between_r( s, first, last ):
    try:
        start = s.rindex( first ) + len( first )
        end = s.rindex( last, start )
        return s[start:end]
    except ValueError:
        return ""

def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def sorting(A,B):
	# sort two lists w.r.t 1st list
	# A=[0,3,1,2],  B=[0,1,2,3]
	# sA=[0,1,2,3],sB=[0,2,3,1]
	sA,sB = zip(*sorted(zip(A,B)))
	return sA,sB

def split_list_by_samecontent(lst):
	#split list into sublist having same value
	#[0,0,10,10,20,20] -> [[0,0],[10,10],[20,20]]
	res = list()
        local_res = list()

        tmp = lst[0]

	for i in range(0,len(lst)):
		if(lst[i] == tmp):
                    local_res.append(lst[i])
                else:
                    res.append(local_res)
                    tmp = lst[i]
                    local_res = list()
                    local_res.append(tmp)


        res.append(local_res)
			
	return res

def split_list_by_indexnumber(lst,indexes):
	#split list into sublist ranging index numbers
	#[0,1,3,2,5,8,9]with[0,2,6] -> [[0,1,3],[2,5,8,9]]
	nextitem = 0
	if(len(indexes) == 1):
		return [lst]
	else:
		res = [[]]
		row = 0
		for i in range(0,len(indexes)):
			if(i <= len(indexes)-2):
				res.append([])
				res[row].append(lst[indexes[i]+nextitem:indexes[i+1]])
				row +=1
			if(i>0):
				nextitem=1
		return res

def divide_list(lst, val):
	return [lst[i]/val for i in range(0,len(lst))]

def multiply_list(lst, val):
	return [lst[i]*val for i in range(0,len(lst))]

def add_list(lst, val):
	return [lst[i]+val for i in range(0,len(lst))]

def createPath(path):
	if not os.path.isdir(path):
            os.mkdir(path)

def confirm(prompt=None, resp=False):
	if prompt is None:
		prompt = 'Confirm?'
		
	if resp:
		prompt = '%s [%s]|%s: ' % (prompt, 'n', 'y')
	else:
		prompt = '%s %s|[%s]: ' % (prompt, 'y', 'n')

	while True:
		ans = raw_input(prompt)
		if not ans:
			return resp
		if ans not in ['y', 'Y', 'n', 'N']:
			print 'please enter y or n.'
			continue
		if ans == 'y' or ans == 'Y':
			return True
		if ans == 'n' or ans == 'N':
			return False

def FCCdislocations(angle):
    if (angle == 180 or angle == 180.0):
        i = 0
    elif (angle == 150 or angle == 150.0):
        i = 1
    elif (angle == 90 or angle == 90.0):
        i = 2
    elif (angle == 120 or angle == 120.0):
        i = 3
    elif (angle == 160 or angle == 160.893):
        i = 4
    elif (angle == 130 or angle == 130.893):
        i = 5
    elif (angle == 109 or angle == 109.107):
        i = 6
    elif (angle == 139 or angle == 139.107):
        i = 7
    else:
        sys.exit("undefined angle...\n")

    ## these values were get from previous study ##
    #     [ 180, 150,  90, 120,160.893,130.893,109.107,139.107]
    LX1 = [-1.0,-1.0, 1.0, 0.0,   -2.0,   -1.0,    1.0,   -1.0]
    LX2 = [ 1.0, 2.0, 1.0, 1.0,    3.0,    5.0,    4.0,    3.0]
    LX3 = [ 0.0,-1.0,-2.0,-1.0,   -1.0,   -4.0,   -5.0,   -2.0]
    LY1 = [ 1.0, 1.0, 1.0, 1.0,    1.0,    1.0,    1.0,    1.0]
    LY2 = [ 1.0, 1.0, 1.0, 1.0,    1.0,    1.0,    1.0,    1.0]
    LY3 = [ 1.0, 1.0, 1.0, 1.0,    1.0,    1.0,    1.0,    1.0]
    LZ1 = [ 1.0, 1.0, 1.0, 2.0,    4.0,    3.0,    3.0,    5.0]
    LZ2 = [ 1.0, 0.0,-1.0,-1.0,    1.0,   -1.0,   -2.0,   -1.0]
    LZ3 = [-2.0,-1.0, 0.0,-1.0,   -5.0,   -2.0,   -1.0,   -4.0]
    RX  = [  10,   6,   6,  10,      4,      2,      2,      4]
    RZ  = [  30,  60,  60,  30,     12,     20,     20,     12]
    RY  = [  10,  10,  10,  10,     10,     10,     10,     10]
    return LX1[i],LX2[i],LX3[i],LY1[i],LY2[i],LY3[i],LZ1[i],LZ2[i],LZ3[i],RX[i],RY[i],RZ[i]

def shearmodulus(angle):
    #shearmodulus in edge (C_{yzyz}) and shearmoulus in screw (C_{xyxy})
    # if(angle == 180 or angle == 180.0 or angle == 120 or angle == 120.0):
    #     return 14.8912594885*10**3, 24.622360001*10**3
    # elif(angle == 90 or angle == 90.0 or angle == 150 or angle == 150.0):
    #     return 24.622360001*10**3, 14.8912594885*10**3
    # elif(angle == 109 or angle == 109.107 or angle == 130 or angle == 130.893):
    #     return 9.02799085925*10**3, 5.12194973353*10**3
    # elif(angle == 139 or angle == 139.107 or angle == 160 or angle == 160.893):
    #     return 5.12194973353*10**3, 9.02799085925*10**3
    # else:
    #     print 'no angle...'
    return 24.622360001*10**3
    
def FCCdislocationsOlmsted(angle):
    if (angle == 180 or angle == 180.0):
        i = 0
    elif (angle == 90 or angle == 90.0):
        i = 1
    else:
        sys.exit("undefined angle...\n")
    ## these values were get from Olmsted paper ##
    #[180,90]
    LX1 = [-1.0, 1.0]
    LX2 = [ 1.0, 1.0]
    LX3 = [ 0.0,-2.0]
    LY1 = [ 1.0, 1.0]
    LY2 = [ 1.0, 1.0]
    LY3 = [ 1.0, 1.0]
    LZ1 = [ 1.0, 1.0]
    LZ2 = [ 1.0,-1.0]
    LZ3 = [-2.0, 0.0]
    RX  = [4, 6]
    RZ  = [20,20]
    RY  = [12,12]
    return LX1[i],LX2[i],LX3[i],LY1[i],LY2[i],LY3[i],LZ1[i],LZ2[i],LZ3[i],RX[i],RY[i],RZ[i]

def alldirectories(cwd):
    CWD = os.getcwd()
    os.chdir(cwd)
    dirList =  os.listdir("./")
    dirs = list()
    for d in dirList:
	if(os.path.isdir(d)==1):
            dirs.append(d)
    os.chdir(CWD)
    return dirs

def exist(file):
    result = os.path.exists(file)
    if(result == True):
        return 1
    else:
        return 0

def dexist(dire):
    result = os.path.isdir(dire)
    if(result == True):
        return 1
    else:
        return 0


def avg(x,y,z,step,freq,name):
	tmp = './accumulated_'+name+'.txt'
	if(step%freq == 0):
		avgX = x
		avgY = y
		avgZ = z
		file1= open(tmp,'w')
	else:
		X,Y,Z=loadtxt(tmp, unpack=True)
		file1= open(tmp,'w') #overwrite file if existed#
		if(len(X)!=len(x) or len(Y)!=len(y) or len(Z)!=len(z)):
			sys.exit('different size of date...')
		sumX = map(operator.add,x,X)
		avgX = divide_list(sumX, 2.0)
		sumY = map(operator.add,y,Y)
		avgY = divide_list(sumY, 2.0)
		sumZ = map(operator.add,z,Z)
		avgZ = divide_list(sumZ, 2.0)

	for i in range(0,len(avgX)):		
		file1.write(str(avgX[i])+' '+str(avgY[i])+' '+str(avgZ[i])+'\n')
	file1.close()
	
	return avgX,avgY,avgZ

def registry_datasort(posfile,dislfile):

    step1,entry1, x, y, z = loadtxt(posfile, unpack=True)
    step2,entry2,dx,dy,dz = loadtxt(dislfile, unpack=True)

    for i in range(0,len(step1)):
    	    if(step1[i]!=step2[i]):
		    sys.exit("steps are different ")
		    
    steps1,index1 = split_list_by_samecontent(step1)
    xs = split_list_by_indexnumber(x,index1)
    zs = split_list_by_indexnumber(z,index1)    


    steps2,index2 = split_list_by_samecontent(step2)
    dxs = split_list_by_indexnumber(dx,index2)
    dzs = split_list_by_indexnumber(dz,index2)

    return steps1,zs,xs,dzs,dxs

def make_choosing_option(dirs):
	for i in range(0,len(dirs)):
		print str(i)+') '+dirs[i]
	numdirs = raw_input("What directory are you interesting? ")
	list_numfolders = numdirs.split(',')
	selected_dirs = list()
	for i,idd in enumerate(list_numfolders):
		wd = './'+dirs[int(idd)]
		selected_dirs.append(wd)
	return selected_dirs

def dislo_pbc(left_length,right_length,coords):
    ##
    #          ^ b
    # <-left-> | <-right->
    # +------------------+
    # |        |         |
    # |       0+---*-*---|--> a
    # |                  |
    # +------------------+
    ##
    rcut = 8
    coord = list()
    #filter out the phonon scattering effect...
    for i in range(0,len(coords)): 
        for j in range(0,len(coords)):
            distance=sqrt((coords[i,0]-coords[j,0])**2+(coords[i,1]-coords[j,1])**2+(coords[i,2]-coords[j,2])**2)
            if (i!=j and distance<rcut):
                coord.append(coords[i,2])
                break
                
    if(len(coord) > 0):
        max_a = max(coord)
        min_a = min(coord)
        len_a = max_a - min_a
        total_length = left_length + right_length
        if(0<max_a<right_length) and (len_a > total_length/2.0):
            # pbc is considered..
            MIN_A = max_a - total_length
            MAX_A = min_a
        else:
            MIN_A = min_a
            MAX_A = max_a
    else:
        MIN_A,MAX_A = 0.,0.
    return MIN_A,MAX_A

def concerningpbc(length,step,pos,skip):
    STEP,POS = list(),list()
    for i in range(0,len(step)):
        if(skip != 'none'):
            if (skip[0]>pos[i] or pos[i]>skip[1]):
                STEP.append(step[i])
                POS.append(pos[i])
        else:
            STEP.append(step[i])
            POS.append(pos[i])
    count = 0        
    mod_step,mod_pos = list(),list()
    for i in range(0, len(STEP)-1):
        curr_pos = POS[i]
        next_pos = POS[i+1]

        if( abs(next_pos-curr_pos)>length/2 and next_pos>curr_pos ):
            count += 1
        elif( abs(next_pos-curr_pos)>length/2 and next_pos<curr_pos):
            count -= 1
        lenZ = length*count
        mod_pos.append(next_pos-lenZ)
        mod_step.append(STEP[i+1])
    return mod_step,mod_pos

def filterDisloRange(st,posi,minposi,maxposi):
    step,position = list(),list()
    for i in range(0,len(posi)):
        if not ( maxposi > posi[i] > minposi ):
            step.append(st[i])
            position.append(posi[i])
    return step,position

def filterDisloFuzzy(st,pos,amount):
    step,position = list(),list()
    for i in range(0,len(pos)-1):
        dz = abs(pos[i+1]-pos[i])
        if(dz < amount):
            step.append(st[i])
            position.append(pos[i])
    return step,position

def dislo_vel(step,pos,From):
    S,P = list(),list()
    for i in range(0,len(pos)):
        if(step[i]>From):
            S.append(step[i])
            P.append(pos[i])

    slope, intercept, r_value, p_value, std_err = stats.linregress(S,P)
    return slope,intercept,std_err,S,P

def latticeConstants(tmp):
    # with respect to A0=4.04527 
    if ((tmp==10)or(tmp==10.0)):
        constant = 1.00029548383
    elif ((tmp==40)or(tmp==40.0)):        
        constant = 1.0011238911
    elif ((tmp==100)or(tmp==100.0)):
        constant = 1.00261
    elif ((tmp==200)or(tmp==200.0)):
        constant = 1.00477
    elif ((tmp==300)or(tmp==300.0)):
        constant =  1.00696
    else:
        print 'WARNING: no lattice parameter of the such temperature'
        constant = 1.0
    return constant

def latticeConstantsErcolessi(temperatures):
    # with respect to A0=4.04527 
    if(int(temperatures)==0):
        constant = 1
    elif(int(temperatures)==100):
        constant = 0.998804705472
    elif(int(temperatures)==200):
        constant = 1.00037958125
    elif(int(temperatures)==300):
        constant =  1.00177879382
    else:
        sys.exit('no lattice parameter of the such temperature...')
    return constant

def material_parameters(potential_name):
    if('lev' in potential_name):
        A = 4.04527
        mu = 24622360001.0
    elif('ssi' in potential_name):
        A = 4.032
        mu = 32.66*10**9
    elif('lloy' in potential_name):
        A = 4.05639500435681
        mu = 28312787000.0
    else:
        sys.exit('unknown potential name: '+potential_name)
    return A,mu

def file_parameters(potential_name,density):
    if('lev' in potential_name):
        template_name = ['/scratch/jcho/templates/barnett_templates/template_90.input.barnett',\
                         '/scratch/jcho/templates/barnett_templates/template_109.107.input.barnett',\
                         '/scratch/jcho/templates/barnett_templates/template_120.input.barnett',\
                         '/scratch/jcho/templates/barnett_templates/template_130.893.input.barnett',\
                         '/scratch/jcho/templates/barnett_templates/template_139.107.input.barnett',\
                         '/scratch/jcho/templates/barnett_templates/template_150.input.barnett',\
                         '/scratch/jcho/templates/barnett_templates/template_160.893.input.barnett',\
                         '/scratch/jcho/templates/barnett_templates/template_180.input.barnett']
        mobility_name = '/scratch/jcho/coupling_straight_dislo/simulations/mobility_law/mobility.mob'
        potential_name = '/scratch/jcho/Al_mm.eam.fs' 
    elif('ssi' in potential_name):
        template_name = ['/scratch/jcho/templates/barnett_templates_Ercolessi/template_90.input.barnett.ercolessi',\
                         '/scratch/jcho/templates/barnett_templates_Ercolessi/template_109.107.input.barnett.ercolessi',\
                         '/scratch/jcho/templates/barnett_templates_Ercolessi/template_120.input.barnett.ercolessi',\
                         '/scratch/jcho/templates/barnett_templates_Ercolessi/template_130.893.input.barnett.ercolessi',\
                         '/scratch/jcho/templates/barnett_templates_Ercolessi/template_139.107.input.barnett.ercolessi',\
                         '/scratch/jcho/templates/barnett_templates_Ercolessi/template_150.input.barnett.ercolessi',\
                         '/scratch/jcho/templates/barnett_templates_Ercolessi/template_160.893.input.barnett.ercolessi',\
                         '/scratch/jcho/templates/barnett_templates_Ercolessi/template_180.input.barnett.ercolessi']
        mobility_name = '/scratch/jcho/cadd3d_implementations/mobility_law/Ercolessi_Al/mobility_Ercolessi.mob'
        potential_name = '/scratch/jcho/Al_ErcolessiAdams.alloy'         
    elif('lloy' in potential_name):
        template_name = ['/scratch/jcho/templates/barnett_templates_Alloy/template_90.input.barnett.alloy',\
                         '/scratch/jcho/templates/barnett_templates_Alloy/template_109.107.input.barnett.alloy',\
                         '/scratch/jcho/templates/barnett_templates_Alloy/template_120.input.barnett.alloy',\
                         '/scratch/jcho/templates/barnett_templates_Alloy/template_130.893.input.barnett.alloy',\
                         '/scratch/jcho/templates/barnett_templates_Alloy/template_139.107.input.barnett.alloy',\
                         '/scratch/jcho/templates/barnett_templates_Alloy/template_150.input.barnett.alloy',\
                         '/scratch/jcho/templates/barnett_templates_Alloy/template_160.893.input.barnett.alloy',\
                         '/scratch/jcho/templates/barnett_templates_Alloy/template_180.input.barnett.alloy']
        mobility_name = '/scratch/jcho/cadd3d_implementations/mobility_law/Ercolessi_AlMg_alloy_'+str(density)+'/mobility_Ercolessi_alloy_'+str(density)+'.mob'
        potential_name = '/scratch/jcho/almg.liu' 
        
    else:
        sys.exit('unknown potential name: '+potential_name)
    return template_name,mobility_name,potential_name
        

    
def color_codes(index):
    colors =  ['blue','DarkOrange','green','red','cyan','grey','magenta','pink','black']
    return colors[index%9]

def symbol_codes(index):
    symbols = ['*','o','s','p','^','h','+','d','v']
    return symbols[index%9]

def change_string(ref_file,ref_string,new_strings):
    temp_file = ref_file+"_temp"
    os.system("cp "+ref_file+" "+temp_file)
    conf_ref = open(temp_file, 'r')
    conf_act = open(ref_file, 'w')
    for lines in ( raw.strip() for raw in conf_ref ):
           if(ref_string in lines):
               #for j in range(0,len(new_strings)):
                   conf_act.write(new_strings+'\n')
           else:
               conf_act.write(lines+'\n')
    conf_act.close()
    conf_ref.close()
    os.system("rm "+temp_file)
    return 1 #need to check whether is successed or not....

def delete_string(ref_file,target_string):
    temp_file = ref_file+"_temp"
    os.system("cp "+ref_file+" "+temp_file)
    conf_ref = open(temp_file, 'r')
    conf_act = open(ref_file, 'w')
    for lines in ( raw.strip() for raw in conf_ref ):
           if(target_string in lines):
               conf_act.write('\n')
           else:
               conf_act.write(lines+'\n')
    conf_act.close()
    conf_ref.close()
    os.system("rm "+temp_file)
    return 1 #need to check whether is successed or not....

def avg_within_steps(steps,step_list,val_list):
    avg_val = 0
    total = 0
    count = 0
    if(len(step_list)!=len(val_list)):
        sys.exit("not same data...")
    for i in range(0,len(step_list)):
        if(steps[1]>step_list[i]>steps[0]):
            count += 1
            total += val_list[i]
    avg_val = total / count
    if(avg_val == 0):
        print 'WARNING: avg value might be wrong...'
    return avg_val

def linecounts(filename):
    lines = 0
    for line in open(filename):
        lines += 1
    return lines

def findIndex(var1,list1,var2,list2):                # 90,list1,200,list2 return 2,3
	Find = 0                                         # +----+-----+-------+
	count = 0                                        # | 90 | 100 |index:0|
	for i in range(0,len(list1)):                    # |    |     |-------|
		if(var1==list1[i]):                          # |    |     |index:1|
			count += 1                               # |    +-----+-------|
			if(Find == 0):                           # |    | 200 |index:2|
				From1= i                             # |    |     |-------|
				Find = 1                             # |    |     |index:3|
	To1 = From1+count-1                              # +----+-----+-------+
	Find,count=0,0                               
        for j in range(0,len(list2)):             
		if(var2==list2[j]):
			if(From1<=j<=To1):
				count += 1
				if(Find==0):
					From2 = j
					Find = 1
	return From2,From2+count-1

def findIndexSingleColume(var1,list1):
	Find = 0                                     
	count = 0                                    
	for i in range(0,len(list1)):                
		if(var1==list1[i] and Find==0):      
                    From=i                
                    Find=1                       
                if (var1!=list1[i] and Find==1):      
                    End= i     
                    Find = 2   
                if (i==len(list1)-1 and Find==1):
                    End= i     
                    Find = 2   
        if(Find!=2):
            sys.exit("not finish to find From and End indices")
	return From,End


def findIndexSingleColumeRange(min_var,max_var,list1):
    Find = 0
    for i in range(0,len(list1)):
        if min_var<=list1[i]<max_var and Find==0:      
            From=i                
            Find=1
        if Find == 1:
            if min_var>list1[i] or list1[i]>max_var:      
                End= i     
                Find = 2
            if (i==len(list1)-1 and Find==1):
                End= i     
                Find = 2
    if(Find!=2):
        sys.exit("From "+str(From)+" and End "+str(End))
    #if(Find==0):
    #    sys.exit("not satisfaction")
    return From,End
