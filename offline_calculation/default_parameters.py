#!/usr/bin/env python
import sys
import os
sys.path.append('./')
from functions import FCCdislocations
import numpy as np
import math

A0 = 4.04527
burg = A0*np.sqrt(2.0)/2.0

class system_parameters:
    def __init__(self):
        self.ang = 90
        self.A0 = A0
        self.RX = 1
        self.RY = 1
        self.RZ = 1
        self.lx1= 1
        self.lx2= 0
        self.lx3= 0
        self.ly1= 0
        self.ly2= 1
        self.ly3= 0
        self.lz1= 0
        self.lz2= 0
        self.lz3= 1
        self.DX = 1
        self.DY = 1
        self.DZ = 1
        self.LX = 1
        self.LY = 1
        self.LZ = 1
        # sigma_xy, sigma_yx, sigma_zy, sigma_yz
        self.stress = [0.0, 0.0, 0.0, 0.0]
        # xy or xz or yz
        self.tilt   = [0.0, 0.0, 0.0]                
        self.burg= burg        
        self.burgedge= burg
        self.burgscrew= 0.0
        self.minseg= 1.0
        self.maxseg= 2.0        
        self.TEMP = 1.0
        self.RCUT = 7.0
        self.pois = 0.281
        self.fix1 = [ 1.0, 0.0, 0.0]
        self.fix2 = [-1.0, 0.0, 0.0]        
        self.lmp_filename = 'lammps.config'
        self.barnett_filename = 'apply_barnett.py'
        self.lib_filename = 'lib_file.config'
        self.que_filename = 'qfile.sub'
        self.ctrl_filename = 'paradis.ctrl'
        self.data_filename = 'nodes.data'
        self.python_filename = 'python_dump.py'


        if('scratch' in os.getcwd()):
            self.template_filename = ['/scratch/jcho/curved_dislocations_coupling/templates/template_90.input.barnett',\
                                      '/scratch/jcho/curved_dislocations_coupling/templates/template_109.107.input.barnett',\
                                      '/scratch/jcho/curved_dislocations_coupling/templates/template_120.input.barnett',\
                                      '/scratch/jcho/curved_dislocations_coupling/templates/template_130.893.input.barnett',\
                                      '/scratch/jcho/curved_dislocations_coupling/templates/template_139.107.input.barnett',\
                                      '/scratch/jcho/curved_dislocations_coupling/templates/template_150.input.barnett',\
                                      '/scratch/jcho/curved_dislocations_coupling/templates/template_160.893.input.barnett',\
                                      '/scratch/jcho/curved_dislocations_coupling/templates/template_180.input.barnett']
            self.mobility_filename = '/scratch/jcho/coupling_straight_dislo/simulations/mobility_law/mobility.mob'
            self.restart_filename = 'none'
            self.ctable_filename = "/scratch/jcho/coupling_straight_dislo/CMTable/fmCorrTbl.txt"
            self.lmp_exe = '/home/jcho/Programs/lammps-28Jun14/src/lmp_ubuntu'
            self.lib_exe = '/home/jcho/lib-dev-simple-dd-move-frank/build/clients/AMEL'            
            self.potential_file = 'Al_mm.eam.fs'
        else:
            self.template_filename = ['/home/jcho/git-DEV/templates/template_90.input.barnett',\
                                      '/home/jcho/git-DEV/templates/template_109.107.input.barnett',\
                                      '/home/jcho/git-DEV/templates/template_120.input.barnett',\
                                      '/home/jcho/git-DEV/templates/template_130.893.input.barnett',\
                                      '/home/jcho/git-DEV/templates/template_139.107.input.barnett',\
                                      '/home/jcho/git-DEV/templates/template_150.input.barnett',\
                                      '/home/jcho/git-DEV/templates/template_160.893.input.barnett',\
                                      '/home/jcho/git-DEV/templates/template_180.input.barnett']
            self.mobility_filename = '/home/jcho/git-DEV/mobility_law/mobility.mob'
            self.restart_filename = 'none'
            self.ctable_filename = "/home/jcho/git-DEV/FMM_table/fmCorrTbl.txt"
            self.lmp_exe = '/home/jcho/Programs/lammps-28Jun14/src/lmp_ubuntu'
            self.lib_exe = '/home/jcho/lib-dev-simple-dd-move-frank/build/clients/AMEL'
            self.potential_file = 'Al_mm.eam.fs'

        self.mu = 24.622360001*10**3*10**6
        self.MPa_2_ev_PER_cubic_angstrom = 0.00000624150974

        self.jobtime = '24:00:00'
        self.quetype = 'local'
        self.detect_freq = 100
        
        ## local variables ##
        self.dirname = 'undefined_path'
        self.jobname = 'undefined'
        self.prc = 8
        self.npr = 1
        self.cwd = 'undefined'
        self.totsteps = 1
        self.dd_boxsize = 1.0
        
    def set_dirname(self,name):
        self.dirname = name

    def add_dirname(self,name):
        return self.dirname+name
    
    def set_data_filename(self,name):
        self.data_filename = name
                
    def set_jobname(self,name):
        self.jobname = name
        
    def set_restart_filename(self,name):
        self.restart_filename = name
        
    def add_jobname(self,name):
        return self.jobname + name        

    def set_lmp_filename(self,name):
        self.lmp_filename = name

    def set_lib_filename(self,name):
        self.lib_filename = name

    def set_python_filename(self,name):
        self.python_filename = name    
    
    def set_cpu(self,num):
        self.prc = num

    def set_numproc(self,num):
        self.npr = num
        
    def set_cwd(self,path):
        self.cwd = path
        
    def set_total_steps(self,num):
        self.totsteps = num

    def mu_in_mpa(self):
        return self.mu/1e+6

    def stress_for_md(self,val):
        return val*self.MPa_2_ev_PER_cubic_angstrom
    
    def set_jobtime(self,val):
        self.jobtime = val

    def set_quetype(self,val):
        self.quetype = val

    def set_detect_freq(self,val):
        self.detect_freq = val
        
    def set_stress(self,val,comp):
        if((comp == 'xy')or(comp == 'XY')):
            self.stress[0] = val
        elif((comp == 'yx')or(comp == 'YX')):
            self.stress[1] = val
        elif((comp == 'zy')or(comp == 'ZY')):
            self.stress[2] = val
        elif((comp == 'yz')or(comp == 'YZ')):
            self.stress[3] = val
        else:
            sys.exit('not support the stress component of '+comp)
            
    def set_tilt(self,val,comp):
        if((comp == 'xy')or(comp == 'XY')):
            self.tilt[0] = val
        elif((comp == 'xz')or(comp == 'XZ')):
            self.tilt[1] = val
        elif((comp == 'yz')or(comp == 'YZ')):
            self.tilt[2] = val
        else:
            sys.exit('not support the tilt component of '+comp)            
        
    def set_fixednodes(self,fix1,fix2):
        self.fix1 = fix1
        self.fix2 = fix2
        
    def set_minseg(self,val):
        self.minseg = val
        
    def set_maxseg(self,val):
        self.maxseg = val        
        
    def set_angle(self,angle):
        lx1,lx2,lx3,ly1,ly2,ly3,lz1,lz2,lz3,rx,ry,rz = FCCdislocations(angle)
        ry = 3*ry
        DX = np.sqrt(lx1**2 + lx2**2 + lx3**2)
        DY = np.sqrt(ly1**2 + ly2**2 + ly3**2)
        DZ = np.sqrt(lz1**2 + lz2**2 + lz3**2)
        LX = A0*rx*DX
        LY = A0*ry*DY
        LZ = A0*rz*DZ
        
        self.ang = angle
        self.RX = rx
        self.RY = ry
        self.RZ = rz
        self.lx1= lx1
        self.lx2= lx2
        self.lx3= lx3
        self.ly1= ly1
        self.ly2= ly2
        self.ly3= ly3
        self.lz1= lz1
        self.lz2= lz2
        self.lz3= lz3
        self.DX = DX
        self.DY = DY
        self.DZ = DZ
        self.LX = LX
        self.LY = LY
        self.LZ = LZ
        
        self.burgedge = burg*np.sin(angle*np.pi/180.0)
        self.burgscrew=-burg*np.cos(angle*np.pi/180.0)
        self.dd_boxsize = math.floor(8.0*(LZ))
    def set_rx(self,val):
        self.RX = val        
        LX = A0*self.RX*self.DX
        self.LX = LX
        
    def set_ry(self,val):
        self.RY = val        
        LY = A0*self.RY*self.DY
        self.LY = LY
        
    def set_rz(self,val):
        self.RZ = val        
        LZ = A0*self.RZ*self.DZ
        self.LZ = LZ           
