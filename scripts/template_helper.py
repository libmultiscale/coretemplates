#!/usr/bin/env python3

import numpy as np
import argparse
import matplotlib.pyplot as plt
from CoreTemplate import template


def main():
    parser = argparse.ArgumentParser(description='CoreTemplateHelper')
    parser.add_argument("filename", type=str,
                        help="The filename of the template to load")
    parser.add_argument("--field", "-f", type=str,
                        help="The filename to plot", default='correction')

    arguments = parser.parse_args()
    core = template.CoreTemplate.load(arguments.filename)

    print(core)

    core.plotMesh(arguments.field)
    plt.show()


if __name__ == "__main__":
    main()
